package com.example.lukas.hangman;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.hamcrest.CustomMatcher;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class UITest {
    private MockWebServer server;

    @Rule
    public final ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class, false, false);

    @Before
    public void before() throws Exception {
        server = new MockWebServer();
        server.enqueue(new MockResponse().setBody("WORD"));
        server.start(8080);

        activityRule.launchActivity(
                new Intent(Intent.ACTION_MAIN).putExtra("word-provider-url", "http://localhost:8080")
        );
    }

    @After
    public void tearDown() throws Exception {
        server.close();
    }

    @Test
    public void guessLetterTextBoxIsEmpty() {
        MainPageObject.clickGuessLetterButton();
        matches(isDisplayed());
    }

    @Test
    public void guessedLetterPrinted() throws InterruptedException {
        MainPageObject.putLetterIntoGuessLetterTextBox('a');
        MainPageObject.closeKeyboard();
        MainPageObject.clickGuessLetterButton();
        // checking if guessed letter appears in textView (textView2) in the top part of the screen
        Matcher<View> viewMatcher = withId(R.id.textView2);
        ViewInteraction viewInteraction = onView(viewMatcher);
        viewInteraction.check(matches(withText(new CustomMatcher<String>("") {
            @Override
            public boolean matches(Object item) {
                return ((String) item).matches("^A\\W*");
            }
        })));
    }

    @Test
    public void inputDisabledAfterDefeat() {
        for (int i = 0; i <= 11; i++) {
            MainPageObject.putLetterIntoGuessLetterTextBox((char) ('Z' + i));
            MainPageObject.clickGuessLetterButton();
        }
        onView(withId(R.id.editText))
                .check(matches(not(isEnabled())));
        onView(withId(R.id.button))
                .check(matches(not(isEnabled())));
    }

    @Test
    public void inputDisabledAfterVictory() {
        char[] word = new char[]{'W', 'O', 'R', 'D'};
        for (char aWord : word) {
            MainPageObject.putLetterIntoGuessLetterTextBox(aWord);
            MainPageObject.clickGuessLetterButton();
        }
        onView(withId(R.id.editText))
                .check(matches(not(isEnabled())));
        onView(withId(R.id.button))
                .check(matches(not(isEnabled())));
    }

    @Test
    public void deviceRotatedDuringGameplay() {
        MainPageObject.putLetterIntoGuessLetterTextBox('A');
        MainPageObject.clickGuessLetterButton();

        activityRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        MainPageObject.putLetterIntoGuessLetterTextBox('B');
        MainPageObject.closeKeyboard();
        MainPageObject.clickGuessLetterButton();

        activityRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
}