package com.example.lukas.hangman;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements MainView {
    private MvpPresenter mPresenter;
    private StateKeeper mStateKeeper;

    private TextView mTitleTextView;
    private TextView mWordTextView;
    private EditText mGuessLetterEditText;
    private ImageView mImageView;
    private Button mButton; //guess letter
    private RelativeLayout mRelativeLayout; //progress bar

    MvpPresenter getPresenter() {
        return mPresenter;
    }

    StateKeeper getStateKeeper() {
        return mStateKeeper;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpObjects();
        setUpViews();
        if (savedInstanceState != null) {
            getStateKeeper().restoreState(savedInstanceState);
            getPresenter().restoreState();
        } else getPresenter().onStartNewGame();
    }

    private void setUpObjects() {
        Model model = new Model(new WordProvider(new AsyncHttpClient(), getUrl()));
        mStateKeeper = new StateKeeper(model);
        mPresenter = new Presenter(model, this);
        Timber.plant(new Timber.DebugTree());
    }

    private void setUpViews() {
        mTitleTextView = (TextView) findViewById(R.id.textView2);
        mWordTextView = (TextView) findViewById(R.id.textView);
        mGuessLetterEditText = (EditText) findViewById(R.id.editText);
        mImageView = (ImageView) findViewById(R.id.imageView);
        mButton = (Button) findViewById(R.id.button);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.loadingPanel);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        getStateKeeper().saveState(savedInstanceState);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void guessLetter(@SuppressWarnings({"SameParameterValue", "UnusedParameters"}) View view) {
        getPresenter().onGuessLetter(mGuessLetterEditText.getText().toString());
        mGuessLetterEditText.setText("");
    }

    public void startNewGame(@SuppressWarnings("SameParameterValue") View view) {
        getPresenter().onStartNewGame();
    }

    @Override
    public void printGuessedLetters(String guessedLetters) {
        mWordTextView.setText(guessedLetters);
    }

    @Override
    public void enableInput() {
        mButton.setEnabled(true);
        mGuessLetterEditText.setEnabled(true);
    }

    @Override
    public void enableProgressBar(boolean enabled) {
        if (enabled)
            mRelativeLayout.setVisibility(View.VISIBLE);
        else mRelativeLayout.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        mTitleTextView.setText(message);
    }

    @Override
    public void changeImage(int id) {
        mImageView.setImageResource(
                getResources().getIdentifier("hang" + id, "drawable", getPackageName()));
    }

    @Override
    public void disableInput() {
        mButton.setEnabled(false);
        mGuessLetterEditText.setEnabled(false);
    }

    private String getUrl() {
        return getIntent().getStringExtra("word-provider-url") == null
                ? BuildConfig.ADDRESS : getIntent().getStringExtra("word-provider-url");
    }
}