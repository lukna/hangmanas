package com.example.lukas.hangman;

interface MvpWordProvider {
    void getWordFromApi(WordReceived callback);

    interface WordReceived {
        void onWordReceived(String word);

        void onFailure(String error, Throwable throwable);
    }
}
