package com.example.lukas.hangman;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.nio.charset.StandardCharsets;

import cz.msebera.android.httpclient.Header;

class WordProvider implements MvpWordProvider {
    private final AsyncHttpClient client;
    private final String url;

    WordProvider(AsyncHttpClient client, String url) {
        this.client = client;
        this.url = url;
        this.client.setMaxRetriesAndTimeout(3, 10000);
        this.client.setEnableRedirects(true);
    }

    @Override
    public void getWordFromApi(WordReceived callback) {
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                callback.onWordReceived(parseResponse(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.onFailure(parseResponse(responseBody), error);
            }

            private String parseResponse(byte[] responseBody) {
                return responseBody != null ? new String(responseBody, StandardCharsets.UTF_8) : "";
            }
        });
    }
}