package com.example.lukas.hangman;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

public class PresenterTest {
    private final spyMainActivity spyView = new spyMainActivity();
    private final spyModel spyModel = new spyModel();
    private final MvpPresenter presenter = new Presenter(spyModel, spyView);

    @Test
    public void getCorrectWord() throws Exception {
        presenter.getCorrectWord();
        assertEquals("word", spyModel.getWord());
    }

    @Test
    public void restoreState() throws Exception {
        presenter.restoreState();
        assertEquals(true, spyView.isGuessedLettersPrinted());
        assertEquals(true, spyView.isImageChanged());
        assertEquals(true, spyView.isInputEnabled());
    }

    @Test
    public void getGuesses() throws Exception {
        presenter.getGuesses();
        assertArrayEquals(new char[]{'A', 'B'}, spyModel.getGuesses());
    }

    @Test
    public void onGuessLetter() throws Exception {
        presenter.onGuessLetter("");
        assertEquals(true, spyView.isMessageShown());
        presenter.onGuessLetter("A");
        assertEquals(true, spyModel.isDoGuessLetterCalled());
    }

    @Test
    public void onStartNewGame() throws Exception {
        presenter.onStartNewGame();
        assertEquals(true, spyView.isProgressBarEnabled());
        assertEquals(true, spyView.isInputDisabled());
        assertEquals(true, spyView.isImageChanged());
        assertEquals(true, spyModel.isStartNewGameCalled());
    }

    private class spyMainActivity implements MainView {
        boolean messageShown = false;
        boolean imageChanged = false;
        boolean inputDisabled = false;
        boolean guessedLettersPrinted = false;
        boolean inputEnabled = false;
        boolean progressBarEnabled = false;

        boolean isMessageShown() {
            return messageShown;
        }

        boolean isImageChanged() {
            return imageChanged;
        }

        boolean isInputDisabled() {
            return inputDisabled;
        }

        boolean isGuessedLettersPrinted() {
            return guessedLettersPrinted;
        }

        boolean isInputEnabled() {
            return inputEnabled;
        }

        boolean isProgressBarEnabled() {
            return progressBarEnabled;
        }

        @Override
        public void showMessage(String message) {
            messageShown = true;
        }

        @Override
        public void changeImage(int id) {
            imageChanged = true;
        }

        @Override
        public void disableInput() {
            inputDisabled = true;
        }

        @Override
        public void printGuessedLetters(String guessedLetters) {
            guessedLettersPrinted = true;
        }

        @Override
        public void enableInput() {
            inputEnabled = true;
        }

        @Override
        public void enableProgressBar(boolean enabled) {
            progressBarEnabled = true;
        }
    }

    private class spyModel implements MvpModel {
        boolean doGuessLetterCalled = false;
        boolean startNewGameCalled = false;

        boolean isDoGuessLetterCalled() {
            return doGuessLetterCalled;
        }

        boolean isStartNewGameCalled() {
            return startNewGameCalled;
        }

        @Override
        public String getWord() {
            return "word";
        }

        @Override
        public char[] getGuessedLetters() {
            return new char[]{'w', 'o', 'r', 'd'};
        }

        @Override
        public void doGuessLetter(char letter) {
            doGuessLetterCalled = true;
        }

        @Override
        public int getNumberOfWrongGuesses() {
            return 2;
        }

        @Override
        public boolean victory() {
            return false;
        }

        @Override
        public boolean gameOver() {
            return false;
        }

        @Override
        public char[] getGuesses() {
            return new char[]{'A', 'B'};
        }

        @Override
        public void startNewGame(GameStartCallback callback) {
            startNewGameCalled = true;
        }

        @Override
        public void restoreState(String word, String guesses) {
        }
    }
}