package com.example.lukas.hangman;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ModelTest {
    private final DummyWordProvider dummyWordProvider = new DummyWordProvider();
    private final MvpModel model = new Model(dummyWordProvider);
    private final DummyGameStartCallback callback = new DummyGameStartCallback();

    @Test
    public void getGuessedLettersTest() {
        assertEquals(null, model.getGuessedLetters());
    }

    @Test
    public void getGuessesTest() {
        assertArrayEquals(new char[32], model.getGuesses());
    }

    @Test
    public void gameOverTest() {
        assertEquals(false, model.gameOver());
    }

    @Test
    public void getNumberOfGuessesTest() {
        assertEquals(0, model.getNumberOfWrongGuesses());
    }

    @Test
    public void wordCompletedTest() {
        assertEquals(false, model.victory());
    }

    @Test
    public void getWordTest() {
        assertEquals("WORD", model.getWord());
    }

    @Test
    public void doGuessLetterTest() {
        model.doGuessLetter('A');
        assertEquals('A', model.getGuesses()[0]);
        assertEquals(1, model.getNumberOfWrongGuesses());
    }

    @Test
    public void restoreStateTest() {
        String word = "WORD", guesses = "ABCD";
        model.restoreState(word, guesses);
        assertEquals(3, model.getNumberOfWrongGuesses());
        for (int i = 0; i < guesses.length(); i++)
            assertEquals(guesses.charAt(i), model.getGuesses()[i]);
        assertArrayEquals(new char[]{'*', '*', '*', 'D'}, model.getGuessedLetters());
        assertEquals(word, model.getWord());
        assertEquals(guesses.charAt(3), model.getGuesses()[3]);
    }

    @Test
    public void startNewGameTest() {
        model.startNewGame(callback);
        assertEquals(true, dummyWordProvider.isGetWordFromApiCalled());
        assertEquals(true, callback.isGameStartedBeenCalled());
        assertEquals(true, callback.isGameFailedToStartBeenCalled());
    }

    private class DummyWordProvider implements MvpWordProvider {
        boolean getWordFromApiCalled = false;

        boolean isGetWordFromApiCalled() {
            return getWordFromApiCalled;
        }

        @Override
        public void getWordFromApi(WordReceived callback) {
            callback.onWordReceived("WORD");
            callback.onFailure("ERROR", new Throwable());
            getWordFromApiCalled = true;
        }
    }

    private class DummyGameStartCallback implements GameStartCallback {
        boolean gameStartedBeenCalled = false;
        boolean gameFailedToStartBeenCalled = false;

        boolean isGameStartedBeenCalled() {
            return gameStartedBeenCalled;
        }

        boolean isGameFailedToStartBeenCalled() {
            return gameFailedToStartBeenCalled;
        }

        @Override
        public void gameStarted() {
            gameStartedBeenCalled = true;
        }

        @Override
        public void gameFailedToStart(String error) {
            gameFailedToStartBeenCalled = true;
        }
    }
}