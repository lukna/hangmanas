package com.example.lukas.hangman;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, manifest = "../../../../../../app/src/main/AndroidManifest.xml")
public class MainActivityTest {
    private SpyPresenter spyPresenter;
    private DummyActivity dummyActivity;
    private SpyStateKeeper spyStateKeeper;

    @Before
    public void setUp() {
        dummyActivity = Robolectric.setupActivity(DummyActivity.class);
        spyPresenter = (SpyPresenter) dummyActivity.getPresenter();
        spyStateKeeper = (SpyStateKeeper) dummyActivity.getStateKeeper();
    }

    @Test
    public void gameStartedOrRestored() throws Exception {
        //new game started
        assertTrue(spyPresenter.onStartNewGameBeenCalled);
        dummyActivity = Robolectric.buildActivity(DummyActivity.class).create(new Bundle()).start().resume().get();
        spyPresenter = (SpyPresenter) dummyActivity.getPresenter();
        spyStateKeeper = (SpyStateKeeper) dummyActivity.getStateKeeper();
        //game restored
        assertTrue(spyPresenter.restoreStateBeenCalled);
        assertTrue(spyStateKeeper.restoreStateBeenCalled);
    }

    @Test
    public void bundlePassedToStateKeeper() throws Exception {
        dummyActivity.onSaveInstanceState(new Bundle());
        assertTrue(spyStateKeeper.saveStateBeenCalled);
    }

    @Test
    public void letterPassedToPresenterOnButtonClick() throws Exception {
        dummyActivity.guessLetter(null);
        assertTrue(spyPresenter.onGuessLetterBeenCalled);
    }

    @Test
    public void startNewGameCalledOnButtonClick() throws Exception {
        dummyActivity.startNewGame(null);
        assertTrue(spyPresenter.onStartNewGameBeenCalled);
    }

    @Test
    public void guessedLettersPrinted() throws Exception {
        dummyActivity.printGuessedLetters("TEST");
        TextView textView = (TextView) dummyActivity.findViewById(R.id.textView);
        assertEquals("TEST", textView.getText().toString());
    }

    @Test
    public void inputEnabled() throws Exception {
        dummyActivity.enableInput();
        Button guessLetterButton = (Button) dummyActivity.findViewById(R.id.button);
        EditText guessLetterEditText = (EditText) dummyActivity.findViewById(R.id.editText);
        assertTrue(guessLetterButton.isEnabled());
        assertTrue(guessLetterEditText.isEnabled());
    }

    @Test
    public void progressBarShownOrHidden() throws Exception {
        RelativeLayout mRelativeLayout = (RelativeLayout) dummyActivity.findViewById(R.id.loadingPanel);
        dummyActivity.enableProgressBar(true);
        assertTrue(mRelativeLayout.isShown());
        dummyActivity.enableProgressBar(false);
        assertFalse(mRelativeLayout.isShown());
    }

    @Test
    public void messageDisplayed() throws Exception {
        TextView textView = (TextView) dummyActivity.findViewById(R.id.textView2);
        dummyActivity.showMessage("TEST");
        assertEquals("TEST", textView.getText().toString());
    }

    @Test
    public void imageChanged() throws Exception {
        ImageView imageView = (ImageView) dummyActivity.findViewById(R.id.imageView);
        dummyActivity.changeImage(0);
        assertEquals(R.drawable.hang0, Shadows.shadowOf(imageView.getDrawable()).getCreatedFromResId());
    }

    @Test
    public void inputDisabled() throws Exception {
        Button guessLetterButton = (Button) dummyActivity.findViewById(R.id.button);
        EditText guessLetterEditText = (EditText) dummyActivity.findViewById(R.id.editText);
        dummyActivity.disableInput();
        assertFalse(guessLetterButton.isEnabled());
        assertFalse(guessLetterEditText.isEnabled());
    }

    private static class SpyPresenter implements MvpPresenter {
        boolean onGuessLetterBeenCalled = false;
        boolean onStartNewGameBeenCalled = false;
        boolean restoreStateBeenCalled = false;

        @Override
        public void onGuessLetter(String letter) {
            onGuessLetterBeenCalled = true;
        }

        @Override
        public void onStartNewGame() {
            onStartNewGameBeenCalled = true;
        }

        @Override
        public String getCorrectWord() {
            return "WORD";
        }

        @Override
        public String getGuesses() {
            return "GUESSES";
        }

        @Override
        public void restoreState() {
            restoreStateBeenCalled = true;
        }
    }

    private static class SpyStateKeeper extends StateKeeper {
        boolean saveStateBeenCalled = false;
        boolean restoreStateBeenCalled = false;

        SpyStateKeeper(MvpModel model) {
            super(model);
        }

        void saveState(Bundle stateStorage) {
            saveStateBeenCalled = true;
        }

        void restoreState(Bundle stateStorage) {
            restoreStateBeenCalled = true;
        }
    }

    @SuppressLint("Registered")
    public static class DummyActivity extends MainActivity {
        final SpyPresenter spyPresenter = new SpyPresenter();
        final SpyStateKeeper spyStateKeeper = new SpyStateKeeper(null);

        public MvpPresenter getPresenter() {
            return spyPresenter;
        }

        public StateKeeper getStateKeeper() {
            return spyStateKeeper;
        }
    }
}