package com.example.lukas.hangman;

import android.os.Bundle;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

public class StateKeeperTest {

    private final DummyModel dummyModel = new DummyModel();
    private final StateKeeper stateKeeper = new StateKeeper(dummyModel);
    private final Bundle stateStorage = new Bundle();

    @Test
    public void saveState() throws Exception {
        stateKeeper.saveState(stateStorage);
        assertEquals("WORD", dummyModel.getWord());
        assertArrayEquals(new char[]{'A', 'B', 'C', 'D'}, dummyModel.getGuesses());
    }

    @Test
    public void restoreState() throws Exception {
        stateKeeper.restoreState(null);
        assertEquals(false, dummyModel.isRestoreStateBeenCalled());

        stateKeeper.restoreState(stateStorage);
        assertEquals(true, dummyModel.isRestoreStateBeenCalled());
    }

    private class DummyModel implements MvpModel {

        boolean restoreStateBeenCalled = false;

        @Override
        public String getWord() {
            return "WORD";
        }

        @Override
        public void restoreState(String word, String guesses) {
            restoreStateBeenCalled = true;
        }

        @Override
        public char[] getGuesses() {
            return new char[]{'A', 'B', 'C', 'D'};
        }

        boolean isRestoreStateBeenCalled() {
            return restoreStateBeenCalled;
        }

        @Override
        public char[] getGuessedLetters() {
            return new char[0];
        }

        @Override
        public void doGuessLetter(char letter) {

        }

        @Override
        public int getNumberOfWrongGuesses() {
            return 0;
        }

        @Override
        public boolean victory() {
            return false;
        }

        @Override
        public boolean gameOver() {
            return false;
        }


        @Override
        public void startNewGame(GameStartCallback callback) {
        }
    }
}